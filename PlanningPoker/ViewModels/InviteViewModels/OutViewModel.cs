﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PlanningPoker.ViewModels.InviteViewModels
{
    public class OutViewModel : InviteViewModel
    {
        [StringLength(16, MinimumLength = 3, ErrorMessage = "Login should be between 3 and 16 characters.")]
        public String InvitedLogin { get; set; }
    }
}