﻿using System;

namespace PlanningPoker.ViewModels.InviteViewModels
{
    public class InViewModel : InviteViewModel
    {
        public Guid InviteId { get; set; }
        public String OwnerName { get; set; }
        public String RoomName { get; set; }
    }
}