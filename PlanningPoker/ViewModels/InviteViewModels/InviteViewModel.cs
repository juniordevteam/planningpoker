﻿using System;

namespace PlanningPoker.ViewModels.InviteViewModels
{
    public class InviteViewModel
    {
        public Int32 OwnerId { get; set; }
        public Int32 RoomId { get; set; }
    }
}