﻿using PlanningPoker.Models;
using PlanningPoker.ViewModels.InviteViewModels;
using PlanningPoker.ViewModels.RoomViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace PlanningPoker.ViewModels.AccountViewModels
{
    public class UserProfileViewModel
    {
        public Int32 UserId { get; set; }
        [StringLength(16, MinimumLength = 3, ErrorMessage = "Login should be between 3 and 16 characters.")]
        public String Login { get; set; }
        [StringLength(32, ErrorMessage = "Email should be not more than 32 characters.")]
        public String Email { get; set; }
        public Boolean? Gender { get; set; }
        [StringLength(20, ErrorMessage = "Phone should be not more than 20 characters.")]
        public String Phone { get; set; }
        [StringLength(50, ErrorMessage = "Country should be not more than 50 characters.")]
        public String Country { get; set; }
        public DateTime? Birthday { get; set; }
        public webpages_Roles Role { get; set; }
        public byte[] Avatar { get; set; }
        public HttpPostedFileBase MainDocument { get; set; }
        public IEnumerable<RoomViewModel> RoomList { get; set; }
        public IEnumerable<InViewModel> InviteList { get; set; }
    }
}