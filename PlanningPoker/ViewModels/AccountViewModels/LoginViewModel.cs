﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PlanningPoker.ViewModels.AccountViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Field must be filled.")]
        [StringLength(16, MinimumLength = 3, ErrorMessage = "Login should be between 3 and 16 characters.")]
        public String Login { get; set; }
        [Required(ErrorMessage = "Field must be filled.")]
        [StringLength(128, MinimumLength = 3, ErrorMessage = "Login should be between 3 and 128 characters.")]
        public String Password { get; set; }
    }
}