﻿using PlanningPoker.Models;
using System;
using System.Collections.Generic;

namespace PlanningPoker.ViewModels.GameViewModels
{
    public class GameViewModel
    {
        public Int32 OwnerId { get; set; }
        public Int32 RoomId { get; set; }
        public UserProfile User { get; set; }
        public Story RoomStory { get; set; }
        public List<UserProfile> UserList { get; set; }
        public List<Story> StoryList { get; set; }
    }
}