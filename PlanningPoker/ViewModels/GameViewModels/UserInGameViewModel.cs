﻿using System;

namespace PlanningPoker.ViewModels.GameViewModels
{
    public class UserInGameViewModel
    {
        public String RoomId { get; set; }
        public String UserId { get; set; }
        public String Login { get; set; }
        public String Avatar { get; set; }
        public String ConnectionId { get; set; }
    }
}