﻿using System;

namespace PlanningPoker.ViewModels.GameViewModels
{
    public class EstimateViewModel
    {
        public String OwnerCardId { get; set; }
        public String RoomId { get; set; }
        public String Estimate { get; set; }
    }
}