﻿using System;

namespace PlanningPoker.ViewModels.RoomViewModels
{
    public class RoomViewModel
    {

        public Int32 RoomId { get; set; }
        public Int32 RoomOwnerId { get; set; }
        public String RoomTitle { get; set; }
        public Int32 UsersCount { get; set; }
        public String UserRole { get; set; }
    }
}