﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PlanningPoker.ViewModels.AccountViewModels
{
    public class CreateRoomViewModel
    {
        [StringLength(64, MinimumLength = 3, ErrorMessage = "Login should be between 3 and 64 characters.")]
        public String Title { get; set; }
    }
}