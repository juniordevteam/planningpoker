﻿using Microsoft.Owin;
using Owin;
[assembly: OwinStartup(typeof(PlanningPoker.Startup))]

namespace PlanningPoker
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}