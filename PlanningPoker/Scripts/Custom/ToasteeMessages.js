﻿//***********************
//  options: {
//  type: 'info', 'error', 'success',
//  header: 'your header text',
//  message: 'your message',
//  color: 'text and close button color',
//  background: 'background color',
//  width: takes an integer (default is 150),
//  height: takes an integer (default is 150)
//  }
//**********************


$(document).ready(function () {
    $('').mouseover(function () {
            $('#my-toast-location').toastee({
                type: 'info',
                message: 'If you want edit your information, please click "Edit profile"',
                width: 200,
                height: 160
            });        
    });
});
    function Welcome() {
        $('#my-toast-location').toastee({
            type: 'success',
            message: 'Welcome to Planning Poker!',
            width: 200,
            height: 160
        });
    }
