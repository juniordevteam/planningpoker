﻿$(function () {
    var game = $.connection.gameHub;

    /*Chat*/
    game.client.addMessage = function (login, msg) {
        $('#discussion').append('<li><b>' + htmlEncode(login) + '</b>:'
            + htmlEncode(msg) + '</li>');
    };

    var user = {
        UserId: $('.container-game').attr('data-user-id'),
        RoomId: $('.container-game').attr('data-room-id'),
        Login: $('#user-name').html(),
        Avatar: $('.container-game').attr('data-user-avatar')
    }

    var roomName = $('.container-game').attr('data-room-id');
    var roomOwnerId = $('.container-game').attr('data-owner-id');

    game.client.onConnected = function (userList, storyList) {
        for (var i = 0; i < userList.length; i++) {
            addUser(userList[i], null);

            if (roomOwnerId != userList[i].UserId) {
                addUserPosition(userList[i], null);
            }
        }

        for (var i = 0; i < storyList.length; i++) {
            addStoryToList(storyList[i]);
        }

        $('#discussion').append('<li><b>' + user.Login + '</b>: joined.</li>');
    }

    game.client.onNewUserConnected = function (user) {
        addUser(user, null);

        if (roomOwnerId != user.UserId) {
            addUserPosition(user, null);
        }        

        $('#discussion').append('<li><b>' + user.Login + '</b>: joined.</li>');
    }

    game.client.onUserDisconnected = function (user) {
        $('#' + user.UserId).remove();
    }
    /*End Chat*/

    /*Game*/
    game.client.showdown = function (estimates, userList) {
        clearTimeout(t);
        $(".timer")[0].innerHTML = '01:59';
        $("#timerOn").attr('disabled', false);

        showdown(estimates, userList);
    }
    /*End Game*/

    /*Timer*/
    var t;

    game.client.startTime = function () {
        startTimer();
    }

    game.client.resetTime = function (value) {
        clearTimeout(t);
        $(".timer")[0].innerHTML = value;
        $("#timerOn").attr('disabled', false);

        hideCards();

        $('.mass-card>li.card-list').removeClass('selected-card');
        $('#connectedUsers>tr>td>div.table-value').replaceWith('<div title="estimate" class="table-value"></div>');
    }
    /*End Timer*/

    /* Story */
    game.client.addStory = function (story) {
        addStoryToList(story);
    }

    game.client.setEstimate = function (storyList) {
        $('.story-list').replaceWith(
            '<tbody id="sortable" class="story-list">' +
            '</tbody>'
        );

        for (var i = 0; i < storyList.length; i++) {
            addStoryToList(storyList[i]);
        }
    }

    game.client.deleteStory = function (storyList) {
        $('.story-list').replaceWith(
            '<tbody id="sortable" class="story-list">' +
            '</tbody>'
        );

        for (var i = 0; i < storyList.length; i++) {
            addStoryToList(storyList[i]);
        }
    }
    /* End Story */

    $.connection.hub.start().done(function () {
        game.server.joinRoom(user);

        $('.send-message').click(function () {

            if ($('#message').val() != null) {
                game.server.sendMessage(user.Login, $('#message').val(), roomName);
            }

            $('#message').val('').focus();
        });

        $('#timerOn').click(function () {
            game.server.startTimer(roomName);
        });

        $('#reset-timer').click(function () {
            game.server.resetTimer(roomName, '01:59');
        });

        $('#add-story').click(function () {
            var story = {
                Title: $('#story-name').val(),
                RoomId: roomName
            }

            if (story.Title != '') {
                game.server.addStory(story);
            }

            $('#story-name').val('');
        });

        $('#set-estimate').click(function () {
            var updatedStory = {
                StoryId: $("#select-story tbody tr.active").attr('data-story-id'),
                RoomId: roomName,
                Estimate: $('#estimate').val()
            }

            game.server.setEstimate(updatedStory);
        });

        $('#delete-story').click(function () {
            var deletedStory = {
                StoryId: $("#select-story tbody tr.active").attr('data-story-id'),
                RoomId: roomName
            }

            game.server.deleteStory(deletedStory);
        });

        $('#showdown').click(function () {
            game.server.seeResults(roomName, roomOwnerId);
        });

        $('.mass-card>li.card-list').click(function () {           
            var value = $(this).html();

            if (value == null || value == '') {
                value = '?';
            }

            var estimate = {
                OwnerCardId: user.UserId,
                RoomId: roomName,
                Estimate: value
            }

            game.server.getEstimate(estimate);
        });
    });   

    function showdown(estimates, userList) {
        $('.mass-player').replaceWith(
            '<div class="mass-player">' +
            '</div>'
            );
        $('#connectedUsers').replaceWith(
            '<tbody id="connectedUsers">' +
            '</tbody>'
            );

        for (var i = 0; i < userList.length; i++) {
            for (var j = 0; j < estimates.length; j++) {
                if (estimates[j].OwnerCardId == userList[i].UserId) {
                    addUserPosition(userList[i], estimates[j].Estimate);
                    addUser(userList[i], estimates[j].Estimate);
                }
            }
        }

        $(".card.effect-click").addClass('flipped');
    }

    function hideCards() {
        $(".card.effect-click").removeClass('flipped');
    }

    function addUserPosition(user, estimate) {
        $('.mass-player').append(
            '<div data-card-owner="' + user.UserId + '" class="card effect-click">' +
                    '<div class="open-card">' +
                        '<span class="card_text">' + estimate + '</span>' +
                    '</div>' +
                    '<div class="close-card">' +
                        '<span class="card_text">A</span>' +
                    '</div>' +
                    '<span class="nicname-holder">' + user.Login + '</span>' +
                '</div>'
        );
    }

    function addUser(user, estimate) {
        if (estimate == null) {
            estimate = '';
        }

        var userId = $('.container-game').attr('data-user-id');

        if (userId != user.UserId) {
            $('#connectedUsers').append(
            '<tr id="' + user.UserId + '">' +
                '<td> <img class="avatar-table" /> </td>' +
                '<td>' + user.Login + '</td>' +
                //'<td><button title="mute player" class="button button-table mute-player"></button></td>' +
                '<td><div title="estimate" class="table-value">' + estimate + '</div></td>' +
            '</tr>'
            );
        }
    }

    function addStoryToList(story) {
        if (story.Estimate == null) {
            story.Estimate = '';
        }
        $('.story-list').append(
            '<tr data-story-id="' + story.StoryId + '">' +
                '<td>' + story.Title + '</td>' +
                '<td><div title="estimate" class="table-value">' + story.Estimate + '</div></td>' +
            '</tr>'
            );
    }

    function htmlEncode(value) {
        var encodedValue = $('<div />').text(value).html();

        return encodedValue;
    }

    function startTimer() {
        $("#timerOn").attr('disabled', true);

        var timer = $(".timer");
        var time = timer[0].innerHTML;
        var arr = time.split(":");
        var m = arr[0];
        var s = arr[1];
        if (s == 0) {
            if (m == 0) {
                game.server.seeResults(roomName);
                return;
            }
            m--;
            if (m < 10) m = "0" + m;
            s = 59;
        }
        else s--;
        if (s < 10) s = "0" + s;
        $(".timer")[0].innerHTML = m + ":" + s;
        t = setTimeout(startTimer, 1000);
    }
});