﻿$(document).ready(function () {
    $('#invite-list').load('/Invite/ShowInviteList');

    $('#addPlayer').click(function () {
        var model = {
            InvitedLogin: $('#playerToInvite').val(),
            RoomId: $('.container-game').attr('data-room-id'),
            OwnerId: $('#addPlayer').attr('data-owner-id')
        }

        $.ajax({
            url: '/Invite/SendInvite',
            type: 'POST',
            datatype: 'json',
            data: model,
            success: function (data) {
                SendInviteSuccess();
                $('#playerToInvite').val('');
            },
            error: function () {
                SendInviteFail();
            }
        });
    });

    $(document).delegate(".applyInvite", "click", function () {
        var model = {
            InviteId: $(this).attr('data-invite-id'),
            RoomId: $(this).attr('data-room-id')
        }

        $.ajax({
            url: '/Invite/ApplyInvite',
            type: 'POST',
            datatype: 'json',
            data: model,
            success: function () {
                location.reload();
            }
        });
    });

    $(document).delegate(".declineInvite", "click", function () {
        
        var model = {
            InviteId: $(this).attr('data-invite-id'),
            RoomId: $(this).attr('data-room-id')
        }

        $.ajax({
            url: '/Invite/DeclineInvite',
            type: 'POST',
            datatype: 'json',
            data: model,
            success: function () {
                location.reload();
            }
        });
    });
});

function SendInviteSuccess() {
    $('#notification').toastee({
        type: 'success',
        message: 'Invite has been sended.',
        width: 200,
        height: 150
    });
}

function SendInviteFail() {
    $('#notification').toastee({
        type: 'error',
        message: 'This user does not exsists.',
        width: 200,
        height: 150
    });
}