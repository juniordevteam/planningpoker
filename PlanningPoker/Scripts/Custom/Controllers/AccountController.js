﻿$(document).ready(function () {
    $(document).delegate("#save", "click", function () {
        var model = {
            Login: $('#user-login').val(),
            Country: $('#user-country').val(),
            MainDocument: $('#user-file').val(),
            Phone: $('#user-phone').val(),
            Birthday: $('#user-birthday').val(),
            Email: $('#user-email').val()
        }

        $.ajax({
            url: '/Account/UserProfile',
            type: 'POST',
            datatype: 'json',
            data: model,
            success: function (data) {
                console.log($('#user-avatar').val());
                UpdateSuccess();
            },
            error: function () {

            }
        });
    });    
});

function UpdateSuccess() {
    $('#update-profile-success').toastee({
        type: 'success',
        message: 'Changes has been saved.',
        width: 250,
        height: 200
    });
}