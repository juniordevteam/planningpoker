﻿$(document).ready(function () {
    $('#room-list').load('/Room/ShowRoomList');

    $('#createRoom').click(function () {
        var model = {
            Title: $('#newRoomName').val()
        }

        $.ajax({
            url: '/Room/CreateRoom',
            type: 'POST',
            datatype: 'json',
            data: model,
            success: function (data) {                
                $('#room-list').load('/Room/ShowRoomList');
                $('#newRoomName').val('');
                AddRoomSuccess();
            },
            error: function () {

            }
        });
    });

    $(document).delegate(".delete-table-room", "click", function () {
        if (confirm('Are you sure you want to delete room?')) {

            var roomIdParam = Number($(this).attr('data-id-room'));
            console.log(roomIdParam);

            $.ajax({
                url: '/Room/DeleteRoom',
                type: 'POST',
                data: { 'roomIdParam': roomIdParam },
                success: function () {
                    $('#room-list').load('/Room/ShowRoomList');
                    DeleteRoomSuccess();
                },
                error: function () {

                }
            });
        }
    });

    $(document).delegate(".out-table-room", "click", function () {
        var roomIdParam = Number($(this).attr('data-id-room'));
        console.log(roomIdParam);

        $.ajax({
            url: '/Room/LeaveRoom',
            type: 'POST',
            data: { 'roomIdParam': roomIdParam },
            success: function () {
                $('#room-list').load('/Room/ShowRoomList');
            },
            error: function () {

            }
        });
    });
});

function AddRoomSuccess() {
    $('#my-toast-location').toastee({
        type: 'success',
        message: 'New room has been added.',
        width: 200,
        height: 150
    });
}

function DeleteRoomSuccess() {
    $('#my-toast-location').toastee({
        type: 'success',
        message: 'Room has been deleted.',
        width: 200,
        height: 150
    });
}