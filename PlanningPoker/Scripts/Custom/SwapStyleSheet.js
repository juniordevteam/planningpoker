﻿function swapStyleSheet(sheet) {
    document.getElementById('pagestyle').setAttribute('href', sheet);

    localStorage.setItem("currentTheme", sheet);
}
