﻿(function () {
    $(document).ready(function () {
        getTheme();
    });

    function getTheme() {
        var currentTheme = localStorage.getItem("currentTheme");

        console.log('current theme', currentTheme);

        if (currentTheme) {
            swapStyleSheet(currentTheme);
        }
    }
})();