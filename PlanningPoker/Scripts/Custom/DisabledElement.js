﻿"use strict"

$(document).delegate('#select-story tbody tr', "click", function (e) {
    e.preventDefault();

    $('.disabled-field').prop('disabled', false);
    $("#select-story tbody tr").removeClass('active');
    $(this).addClass('active');
})