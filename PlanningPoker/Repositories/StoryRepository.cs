﻿using PlanningPoker.Models;
using PlanningPoker.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace PlanningPoker.Repositories
{
    public class StoryRepository : ICommonRepository<Story>, IStoryRepository
    {
        private PlanningPokerEntities _dbContext;

        public StoryRepository() { }
        public StoryRepository(PlanningPokerEntities contextParam)
        {
            this._dbContext = contextParam;
        }

        public void Create(Story storyParam)
        {
            _dbContext.Stories.Add(storyParam);
        }

        public void Update(Story storyParam)
        {
            _dbContext.Entry(storyParam).State = EntityState.Modified;
        }

        public Story GetById(Guid idParam)
        {
            return _dbContext.Stories.Find(idParam);
        }

        public IEnumerable<Story> GetAllForRoom(Room room)
        {
            return room.Stories.ToList();
        }

        public void Delete(Story entityToDelete)
        {
            _dbContext.Stories.Remove(entityToDelete);
        }
    }
}