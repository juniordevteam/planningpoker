﻿using System;
using System.Collections.Generic;
using System.Linq;
using PlanningPoker.Models;
using PlanningPoker.Repositories.Interfaces;

namespace PlanningPoker.Repositories
{
    public class InviteRepository : IInviteRepository
    {
        private PlanningPokerEntities _dbContext;

        public InviteRepository() { }
        public InviteRepository(PlanningPokerEntities contextParam)
        {
            this._dbContext = contextParam;
        }
        public void Create(Invite inviteParam)
        {
            _dbContext.Invites.Add(inviteParam);
        }

        public void Delete(Invite inviteParam)
        {
            _dbContext.Invites.Remove(inviteParam);
        }

        public IEnumerable<Invite> GetAllForUser(UserProfile user)
        {
            return user.Invites.ToList();
        }

        public Invite GetById(Guid idParam)
        {
            return _dbContext.Invites.Find(idParam);
        }
    }
}