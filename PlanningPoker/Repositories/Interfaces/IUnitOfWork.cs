﻿using System;

namespace PlanningPoker.Repositories.Interfaces
{
    interface IUnitOfWork : IDisposable
    {
        UserProfileRepository UserProfileRepository { get; }
        RoomRepository RoomRepository { get; }
        InviteRepository InviteRepository { get; }

        void Save();
    }
}
