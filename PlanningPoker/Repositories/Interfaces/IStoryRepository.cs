﻿using PlanningPoker.Models;
using System;

namespace PlanningPoker.Repositories.Interfaces
{
    interface IStoryRepository
    {
        Story GetById(Guid id);
    }
}
