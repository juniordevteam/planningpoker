﻿using PlanningPoker.Models;
using System;
using System.Collections.Generic;

namespace PlanningPoker.Repositories.Interfaces
{
    interface IUserProfileRepository
    {
        void Update(UserProfile userParam);
        UserProfile GetById(Int32 idParam);
        UserProfile GetByLogin(String login);
        IEnumerable<UserProfile> GetAll();
    }
}
