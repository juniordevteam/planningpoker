﻿
namespace PlanningPoker.Repositories.Interfaces
{
    interface ICommonRepository<TEntity> where TEntity : class, new()
    {    
        void Create(TEntity entity);
        void Delete(TEntity entityToDelete);
        void Update(TEntity entityToUpdate);
    }
}
