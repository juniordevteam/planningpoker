﻿using PlanningPoker.Models;
using System.Collections.Generic;

namespace PlanningPoker.Repositories.Interfaces
{
    interface IRoomRepository
    {
        IEnumerable<Room> GetAllForUser(UserProfile user);
        IEnumerable<Room> GetAll();
    }
}
