﻿using PlanningPoker.Models;
using System;
using System.Collections.Generic;

namespace PlanningPoker.Repositories.Interfaces
{
    interface IInviteRepository
    {
        void Create(Invite inviteParam);
        void Delete(Invite inviteParam);
        Invite GetById(Guid idParam);
        IEnumerable<Invite> GetAllForUser(UserProfile user);
    }
}
