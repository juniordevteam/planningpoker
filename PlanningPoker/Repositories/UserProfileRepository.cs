﻿using PlanningPoker.Models;
using PlanningPoker.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace PlanningPoker.Repositories
{
    public class UserProfileRepository : IUserProfileRepository
    {
        private PlanningPokerEntities _dbContext;

        public UserProfileRepository() { }
        public UserProfileRepository(PlanningPokerEntities contextParam)
        {
            this._dbContext = contextParam;
        }

        public void Delete(UserProfile entityToDelete)
        {
            throw new NotImplementedException();
        }

        public UserProfile GetById(Int32 idParam)
        {
            return _dbContext.UserProfiles.Find(idParam);
        }
        public IEnumerable<UserProfile> GetAll()
        {
            return _dbContext.UserProfiles.ToList();
        }

        public void Update(UserProfile userParam)
        {
            _dbContext.Entry(userParam).State = EntityState.Modified;
        }

        public UserProfile GetByLogin(String login)
        {
            return _dbContext.UserProfiles.Where(u => u.Login.Equals(login)).SingleOrDefault();
        }
    }
}