﻿using PlanningPoker.Models;
using PlanningPoker.Repositories.Interfaces;
using System;

namespace PlanningPoker.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private PlanningPokerEntities _context = new PlanningPokerEntities();
        private UserProfileRepository _userProfileRepository;
        private RoomRepository _roomRepository;
        private InviteRepository _inviteRepository;
        private StoryRepository _storyRepository;

        private Boolean disposed = false;

        public UserProfileRepository UserProfileRepository
        {
            get
            {
                if (this._userProfileRepository == null)
                    this._userProfileRepository = new UserProfileRepository(_context);

                return _userProfileRepository;
            }
        }

        public RoomRepository RoomRepository
        {
            get
            {
                if (this._roomRepository == null)
                    this._roomRepository = new RoomRepository(_context);

                return _roomRepository;
            }
        }
        public InviteRepository InviteRepository
        {
            get
            {
                if (this._inviteRepository == null)
                    this._inviteRepository = new InviteRepository(_context);

                return _inviteRepository;
            }
        }

        public StoryRepository StoryRepository
        {
            get
            {
                if (this._storyRepository == null)
                    this._storyRepository = new StoryRepository(_context);

                return _storyRepository;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
        
        private void Dispose(Boolean disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}