﻿using PlanningPoker.Models;
using PlanningPoker.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace PlanningPoker.Repositories
{
    public class RoomRepository : ICommonRepository<Room>, IRoomRepository
    {
        private PlanningPokerEntities _dbContext;

        public RoomRepository() { }
        public RoomRepository(PlanningPokerEntities contextParam)
        {
            this._dbContext = contextParam;
        }

        public void Create(Room roomParam)
        {
            _dbContext.Rooms.Add(roomParam);
        }

        public void Update(Room roomParam)
        {
            _dbContext.Entry(roomParam).State = EntityState.Modified;
        }

        public Room GetById(Int32 idParam)
        {
            return _dbContext.Rooms.Find(idParam);
        }

        public IEnumerable<Room> GetAllForUser(UserProfile user)
        {
            return user.Rooms.ToList();
        }

        public void Delete(Room roomParam)
        {
            _dbContext.Rooms.Remove(roomParam);
        }

        public IEnumerable<Room> GetAll()
        {
            return _dbContext.Rooms.ToList();
        }
    }
}