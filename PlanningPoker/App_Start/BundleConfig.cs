﻿using System.Web.Optimization;

namespace PlanningPoker.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/CSS/css").Include(
                "~/Content/CSS/styles.css"));

            bundles.Add(new ScriptBundle("~/Scripts/Vendor/jQuery/js").Include(
                "~/Scripts/Vendor/jQuery/jquery-{version}.js",
                "~/Scripts/Vendor/jQuery/jquery.*"                              
                ));

            bundles.Add(new ScriptBundle("~/Scripts/Vendor/jQuery-ui").Include(
                "~/Scripts/Vendor/jQuery/jquery-ui-{version}.js"
                ));

            bundles.Add(new ScriptBundle("~/Scripts/Vendor/Toastee").Include(
                "~/Scripts/Vendor/Toastee/jquery.toastee.0.1.js"
                ));

            bundles.Add(new ScriptBundle("~/Scripts/Custom/ToasteeMessages").Include(
                "~/Scripts/Custom/ToasteeMessages.js"
                ));

            bundles.Add(new ScriptBundle("~/Scripts/Custom/js").Include(
                "~/Scripts/Custom/SwapStyleSheet.js",
                "~/Scripts/Custom/Preloader.js",
                "~/Scripts/Custom/OnLoad.js"
                ));

            bundles.Add(new ScriptBundle("~/Scripts/Custom/Controllers").Include(
                "~/Scripts/Custom/Controllers/*.js"
                ));
        }
    }
}