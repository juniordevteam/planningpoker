﻿using PlanningPoker.Repositories;
using PlanningPoker.ViewModels.GameViewModels;
using System;
using System.Web.Mvc;
using WebMatrix.WebData;
using System.Linq;

namespace PlanningPoker.Controllers
{
    [Authorize]
    public class GameController : Controller
    {
        private UnitOfWork _unitOfWork = new UnitOfWork();

        public ActionResult GameOwnerView(int idRoom)
        {
            var room = _unitOfWork.RoomRepository.GetById(idRoom);
            var usersFromRoom = room.UserProfiles.ToList();
            var currentUser = _unitOfWork.UserProfileRepository.GetById(WebSecurity.CurrentUserId);

            var model = new GameViewModel();

            model.RoomId = idRoom;
            model.OwnerId = WebSecurity.CurrentUserId;
            model.UserList = usersFromRoom;
            model.User = currentUser;

            return View(model);
        }

        public ActionResult GameUserView(int idRoom)
        {
            var room = _unitOfWork.RoomRepository.GetById(idRoom);
            var usersFromRoom = room.UserProfiles.ToList();
            var currentUser = _unitOfWork.UserProfileRepository.GetById(WebSecurity.CurrentUserId);
            var ownerId = _unitOfWork.UserProfileRepository.GetById((int)room.OwnerId).UserId;

            var model = new GameViewModel();

            model.RoomId = idRoom;
            model.OwnerId = ownerId;
            model.UserList = usersFromRoom;
            model.User = currentUser;

            return View(model);
        }

        protected override void Dispose(Boolean disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}