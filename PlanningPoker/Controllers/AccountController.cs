﻿using PlanningPoker.Repositories;
using PlanningPoker.ViewModels.AccountViewModels;
using System;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;
using System.IO;
using System.Drawing;

namespace PlanningPoker.Controllers
{
    public class AccountController : Controller
    {
        private UnitOfWork _unitOfWork = new UnitOfWork();

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, String returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (WebSecurity.Login(model.Login, model.Password))
                {
                    if (returnUrl != null)
                    {
                        return Redirect(returnUrl);
                    }
                    return RedirectToAction("UserProfile");
                }
                else
                {
                    ModelState.AddModelError("", "Sorry the username or password is invalid");
                    return View(model);
                }
            }

            ModelState.AddModelError("", "Sorry the username od password is invalid");
            return View(model);
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    WebSecurity.CreateUserAndAccount(model.Login, model.Password);
                    return RedirectToAction("Login");
                }
                catch (MembershipCreateUserException)
                {
                    ModelState.AddModelError("", "Sorry the username alredy exists");
                    return View(model);
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult Logout()
        {
            WebSecurity.Logout();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [Authorize]
        public ActionResult UserProfile()
        {
            var userProfile = _unitOfWork.UserProfileRepository.GetById(WebSecurity.CurrentUserId);                       

            var model = new UserProfileViewModel();

            model.Login = User.Identity.Name;
            model.Email = userProfile.Email;
            model.Gender = userProfile.Gender;
            model.Phone = userProfile.Phone;
            model.Country = userProfile.Country;
            model.Birthday = userProfile.Birthday;

            if (userProfile.Avatar != null)
            {
                ShowAvatar(userProfile.Avatar);
            }            

            return View(model);
        }

        public Image ShowAvatar(byte[] avatar)
        {
            MemoryStream stm = new MemoryStream(avatar);
            stm.Position = 0;
            return Image.FromStream(stm);
        }

        [HttpPost]
        public ActionResult UserProfile(UserProfileViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userProfile = _unitOfWork.UserProfileRepository.GetById(WebSecurity.CurrentUserId);

                userProfile.Login = model.Login;
                userProfile.Email = model.Email;
                userProfile.Gender = model.Gender;
                userProfile.Phone = model.Phone;
                userProfile.Country = model.Country;
                userProfile.Birthday = model.Birthday;

                if (model.MainDocument != null)
                {
                    int intLength = Convert.ToInt32(model.MainDocument.InputStream.Length);
                    byte[] fileData = new byte[intLength];
                    model.MainDocument.InputStream.Read(fileData, 0, intLength);
                    userProfile.Avatar = fileData;
                }

                _unitOfWork.UserProfileRepository.Update(userProfile);
                _unitOfWork.Save();
            }

            return View(model);
        }

        protected override void Dispose(Boolean disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}