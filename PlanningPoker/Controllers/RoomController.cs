﻿using PlanningPoker.Models;
using PlanningPoker.Repositories;
using PlanningPoker.ViewModels.AccountViewModels;
using System.Web.Mvc;
using WebMatrix.WebData;
using System;
using System.Linq;
using PlanningPoker.ViewModels.RoomViewModels;

namespace PlanningPoker.Controllers
{
    [Authorize]
    public class RoomController : Controller
    {
        private UnitOfWork _unitOfWork = new UnitOfWork();

        [HttpPost]
        public String CreateRoom(CreateRoomViewModel model)
        {
            if (ModelState.IsValid)
            {
                var room = new Room();
                room.Title = model.Title;
                room.OwnerId = WebSecurity.CurrentUserId;

                _unitOfWork.RoomRepository.Create(room);

                var user = _unitOfWork.UserProfileRepository.GetById(WebSecurity.CurrentUserId);

                user.Rooms.Add(room);
                room.UserProfiles.Add(user);
                
                _unitOfWork.Save();
            }            

            return String.Empty;
        }

        [HttpPost]
        public string DeleteRoom(Int32 roomIdParam)
        {
            var room = _unitOfWork.RoomRepository.GetById(roomIdParam);

            if (room != null)
            {
                _unitOfWork.RoomRepository.Delete(room);
                var user = _unitOfWork.UserProfileRepository.GetById(WebSecurity.CurrentUserId);

                user.Rooms.Remove(room);

                _unitOfWork.Save();
            }            

            return "Deleted!";
        }

        [HttpPost]
        public String LeaveRoom(Int32 roomIdParam)
        {
            var room = _unitOfWork.RoomRepository.GetById(roomIdParam);

            if (room != null)
            {
                var user = _unitOfWork.UserProfileRepository.GetById(WebSecurity.CurrentUserId);

                user.Rooms.Remove(room);

                _unitOfWork.Save();
            }

            return String.Empty;
        }

        public ActionResult ShowRoomList()
        {
            var model = new UserProfileViewModel();

            var userProfile = _unitOfWork.UserProfileRepository.GetById(WebSecurity.CurrentUserId);
            var userRooms = _unitOfWork.RoomRepository.GetAllForUser(userProfile);

            var roomList = userRooms.Select(r => new RoomViewModel()
            {
                RoomId = r.RoomId,
                RoomOwnerId = r.OwnerId ?? 0,
                RoomTitle = r.Title,
                UsersCount = r.UserProfiles.Count,
                UserRole = r.OwnerId == userProfile.UserId ? "owner" : "user"
            });

            model.RoomList = roomList;

            return PartialView(model);
        }

        protected override void Dispose(Boolean disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}