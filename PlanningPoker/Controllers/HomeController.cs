﻿using System.Web.Mvc;

namespace PlanningPoker.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
    }
}