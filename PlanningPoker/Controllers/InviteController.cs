﻿using PlanningPoker.Models;
using PlanningPoker.Repositories;
using PlanningPoker.ViewModels.AccountViewModels;
using PlanningPoker.ViewModels.InviteViewModels;
using System;
using System.Linq;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace PlanningPoker.Controllers
{
    public class InviteController : Controller
    {
        private UnitOfWork _unitOfWork = new UnitOfWork();

        [HttpPost]
        public ActionResult SendInvite(OutViewModel model)
        {
            if (ModelState.IsValid)
            {
                var invite = new Invite();
                var user = _unitOfWork.UserProfileRepository.GetByLogin(model.InvitedLogin);

                if (user == null)
                {
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
                }

                invite.InviteId = Guid.NewGuid();
                invite.OwnerId = model.OwnerId;
                invite.RoomId = model.RoomId;

                _unitOfWork.InviteRepository.Create(invite);

                user.Invites.Add(invite);
                invite.UserProfiles.Add(user);

                _unitOfWork.Save();
            }            

            return new HttpStatusCodeResult(System.Net.HttpStatusCode.OK);
        }

        [HttpPost]
        public String ApplyInvite(InViewModel model)
        {
            var user = _unitOfWork.UserProfileRepository.GetById(WebSecurity.CurrentUserId);
            var invite = _unitOfWork.InviteRepository.GetById(model.InviteId);
            var room = _unitOfWork.RoomRepository.GetById(model.RoomId);

            user.Rooms.Add(room);
            room.UserProfiles.Add(user);

            user.Invites.Remove(invite);
            invite.UserProfiles.Remove(user);
            _unitOfWork.InviteRepository.Delete(invite);

            _unitOfWork.Save();

            return String.Empty;
        }

        [HttpPost]
        public String DeclineInvite(InViewModel model)
        {
            var user = _unitOfWork.UserProfileRepository.GetById(WebSecurity.CurrentUserId);
            var invite = _unitOfWork.InviteRepository.GetById(model.InviteId);

            user.Invites.Remove(invite);
            invite.UserProfiles.Remove(user);
            _unitOfWork.InviteRepository.Delete(invite);

            _unitOfWork.Save();

            return String.Empty;
        }

        public ActionResult ShowInviteList()
        {
            var model = new UserProfileViewModel();

            var userProfile = _unitOfWork.UserProfileRepository.GetById(WebSecurity.CurrentUserId);
            var invites = _unitOfWork.InviteRepository.GetAllForUser(userProfile);
            var allRooms = _unitOfWork.RoomRepository.GetAll();
            var allUsers = _unitOfWork.UserProfileRepository.GetAll();

            var inviteList = invites.Select(i => new InViewModel()
            {
                InviteId = i.InviteId,
                OwnerId = (Int32)i.OwnerId,
                RoomId = (Int32)i.RoomId,
                OwnerName = allUsers.Where(o => o.UserId == i.OwnerId).Single().Login,
                RoomName = allRooms.Where(r => r.RoomId == i.RoomId).Single().Title
            });

            model.InviteList = inviteList;

            return PartialView(model);
        }

        protected override void Dispose(Boolean disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}