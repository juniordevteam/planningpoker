
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 06/19/2016 20:56:07
-- Generated from EDMX file: L:\planningpoker\PlanningPoker\Models\PPModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [PlanningPoker];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Stories_Rooms]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Stories] DROP CONSTRAINT [FK_Stories_Rooms];
GO
IF OBJECT_ID(N'[dbo].[FK_UserProfile_Invites_Invites]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserProfile_Invites] DROP CONSTRAINT [FK_UserProfile_Invites_Invites];
GO
IF OBJECT_ID(N'[dbo].[FK_UserProfile_Invites_UserProfiles]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserProfile_Invites] DROP CONSTRAINT [FK_UserProfile_Invites_UserProfiles];
GO
IF OBJECT_ID(N'[dbo].[FK_UserProfile_Room_Rooms]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserProfile_Room] DROP CONSTRAINT [FK_UserProfile_Room_Rooms];
GO
IF OBJECT_ID(N'[dbo].[FK_UserProfile_Room_UserProfiles]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserProfile_Room] DROP CONSTRAINT [FK_UserProfile_Room_UserProfiles];
GO
IF OBJECT_ID(N'[dbo].[FK_webpages_UsersInRoles_UserProfiles]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[webpages_UsersInRoles] DROP CONSTRAINT [FK_webpages_UsersInRoles_UserProfiles];
GO
IF OBJECT_ID(N'[dbo].[FK_webpages_UsersInRoles_webpages_Roles]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[webpages_UsersInRoles] DROP CONSTRAINT [FK_webpages_UsersInRoles_webpages_Roles];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Invites]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Invites];
GO
IF OBJECT_ID(N'[dbo].[Rooms]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Rooms];
GO
IF OBJECT_ID(N'[dbo].[Stories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Stories];
GO
IF OBJECT_ID(N'[dbo].[sysdiagrams]', 'U') IS NOT NULL
    DROP TABLE [dbo].[sysdiagrams];
GO
IF OBJECT_ID(N'[dbo].[UserProfile_Invites]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserProfile_Invites];
GO
IF OBJECT_ID(N'[dbo].[UserProfile_Room]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserProfile_Room];
GO
IF OBJECT_ID(N'[dbo].[UserProfiles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserProfiles];
GO
IF OBJECT_ID(N'[dbo].[webpages_Membership]', 'U') IS NOT NULL
    DROP TABLE [dbo].[webpages_Membership];
GO
IF OBJECT_ID(N'[dbo].[webpages_OAuthMembership]', 'U') IS NOT NULL
    DROP TABLE [dbo].[webpages_OAuthMembership];
GO
IF OBJECT_ID(N'[dbo].[webpages_Roles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[webpages_Roles];
GO
IF OBJECT_ID(N'[dbo].[webpages_UsersInRoles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[webpages_UsersInRoles];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Invites'
CREATE TABLE [dbo].[Invites] (
    [InviteId] uniqueidentifier  NOT NULL,
    [RoomId] int  NULL,
    [OwnerId] int  NULL
);
GO

-- Creating table 'Rooms'
CREATE TABLE [dbo].[Rooms] (
    [RoomId] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(64)  NULL,
    [Description] nvarchar(256)  NULL,
    [OwnerId] int  NULL
);
GO

-- Creating table 'Stories'
CREATE TABLE [dbo].[Stories] (
    [StoryId] uniqueidentifier  NOT NULL,
    [Title] nvarchar(500)  NULL,
    [Description] varchar(max)  NULL,
    [Estimate] int  NULL,
    [RoomId] int  NULL
);
GO

-- Creating table 'sysdiagrams'
CREATE TABLE [dbo].[sysdiagrams] (
    [name] nvarchar(128)  NOT NULL,
    [principal_id] int  NOT NULL,
    [diagram_id] int IDENTITY(1,1) NOT NULL,
    [version] int  NULL,
    [definition] varbinary(max)  NULL
);
GO

-- Creating table 'UserProfiles'
CREATE TABLE [dbo].[UserProfiles] (
    [UserId] int IDENTITY(1,1) NOT NULL,
    [Login] nvarchar(16)  NULL,
    [Email] nvarchar(32)  NULL,
    [Gender] bit  NULL,
    [Phone] nvarchar(20)  NULL,
    [Country] nvarchar(50)  NULL,
    [Birthday] datetime  NULL,
    [Avatar] varbinary(max)  NULL,
    [ConnectionId] varchar(max)  NULL
);
GO

-- Creating table 'webpages_Membership'
CREATE TABLE [dbo].[webpages_Membership] (
    [UserId] int  NOT NULL,
    [CreateDate] datetime  NULL,
    [ConfirmationToken] nvarchar(128)  NULL,
    [IsConfirmed] bit  NULL,
    [LastPasswordFailureDate] datetime  NULL,
    [PasswordFailuresSinceLastSuccess] int  NOT NULL,
    [Password] nvarchar(128)  NOT NULL,
    [PasswordChangedDate] datetime  NULL,
    [PasswordSalt] nvarchar(128)  NOT NULL,
    [PasswordVerificationToken] nvarchar(128)  NULL,
    [PasswordVerificationTokenExpirationDate] datetime  NULL
);
GO

-- Creating table 'webpages_OAuthMembership'
CREATE TABLE [dbo].[webpages_OAuthMembership] (
    [Provider] nvarchar(30)  NOT NULL,
    [ProviderUserId] nvarchar(100)  NOT NULL,
    [UserId] int  NOT NULL
);
GO

-- Creating table 'webpages_Roles'
CREATE TABLE [dbo].[webpages_Roles] (
    [RoleId] int IDENTITY(1,1) NOT NULL,
    [RoleName] nvarchar(256)  NOT NULL
);
GO

-- Creating table 'UserProfile_Invites'
CREATE TABLE [dbo].[UserProfile_Invites] (
    [Invites_InviteId] uniqueidentifier  NOT NULL,
    [UserProfiles_UserId] int  NOT NULL
);
GO

-- Creating table 'UserProfile_Room'
CREATE TABLE [dbo].[UserProfile_Room] (
    [Rooms_RoomId] int  NOT NULL,
    [UserProfiles_UserId] int  NOT NULL
);
GO

-- Creating table 'webpages_UsersInRoles'
CREATE TABLE [dbo].[webpages_UsersInRoles] (
    [UserProfiles_UserId] int  NOT NULL,
    [webpages_Roles_RoleId] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [InviteId] in table 'Invites'
ALTER TABLE [dbo].[Invites]
ADD CONSTRAINT [PK_Invites]
    PRIMARY KEY CLUSTERED ([InviteId] ASC);
GO

-- Creating primary key on [RoomId] in table 'Rooms'
ALTER TABLE [dbo].[Rooms]
ADD CONSTRAINT [PK_Rooms]
    PRIMARY KEY CLUSTERED ([RoomId] ASC);
GO

-- Creating primary key on [StoryId] in table 'Stories'
ALTER TABLE [dbo].[Stories]
ADD CONSTRAINT [PK_Stories]
    PRIMARY KEY CLUSTERED ([StoryId] ASC);
GO

-- Creating primary key on [diagram_id] in table 'sysdiagrams'
ALTER TABLE [dbo].[sysdiagrams]
ADD CONSTRAINT [PK_sysdiagrams]
    PRIMARY KEY CLUSTERED ([diagram_id] ASC);
GO

-- Creating primary key on [UserId] in table 'UserProfiles'
ALTER TABLE [dbo].[UserProfiles]
ADD CONSTRAINT [PK_UserProfiles]
    PRIMARY KEY CLUSTERED ([UserId] ASC);
GO

-- Creating primary key on [UserId] in table 'webpages_Membership'
ALTER TABLE [dbo].[webpages_Membership]
ADD CONSTRAINT [PK_webpages_Membership]
    PRIMARY KEY CLUSTERED ([UserId] ASC);
GO

-- Creating primary key on [Provider], [ProviderUserId] in table 'webpages_OAuthMembership'
ALTER TABLE [dbo].[webpages_OAuthMembership]
ADD CONSTRAINT [PK_webpages_OAuthMembership]
    PRIMARY KEY CLUSTERED ([Provider], [ProviderUserId] ASC);
GO

-- Creating primary key on [RoleId] in table 'webpages_Roles'
ALTER TABLE [dbo].[webpages_Roles]
ADD CONSTRAINT [PK_webpages_Roles]
    PRIMARY KEY CLUSTERED ([RoleId] ASC);
GO

-- Creating primary key on [Invites_InviteId], [UserProfiles_UserId] in table 'UserProfile_Invites'
ALTER TABLE [dbo].[UserProfile_Invites]
ADD CONSTRAINT [PK_UserProfile_Invites]
    PRIMARY KEY CLUSTERED ([Invites_InviteId], [UserProfiles_UserId] ASC);
GO

-- Creating primary key on [Rooms_RoomId], [UserProfiles_UserId] in table 'UserProfile_Room'
ALTER TABLE [dbo].[UserProfile_Room]
ADD CONSTRAINT [PK_UserProfile_Room]
    PRIMARY KEY CLUSTERED ([Rooms_RoomId], [UserProfiles_UserId] ASC);
GO

-- Creating primary key on [UserProfiles_UserId], [webpages_Roles_RoleId] in table 'webpages_UsersInRoles'
ALTER TABLE [dbo].[webpages_UsersInRoles]
ADD CONSTRAINT [PK_webpages_UsersInRoles]
    PRIMARY KEY CLUSTERED ([UserProfiles_UserId], [webpages_Roles_RoleId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [RoomId] in table 'Stories'
ALTER TABLE [dbo].[Stories]
ADD CONSTRAINT [FK_Stories_Rooms]
    FOREIGN KEY ([RoomId])
    REFERENCES [dbo].[Rooms]
        ([RoomId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Stories_Rooms'
CREATE INDEX [IX_FK_Stories_Rooms]
ON [dbo].[Stories]
    ([RoomId]);
GO

-- Creating foreign key on [Invites_InviteId] in table 'UserProfile_Invites'
ALTER TABLE [dbo].[UserProfile_Invites]
ADD CONSTRAINT [FK_UserProfile_Invites_Invites]
    FOREIGN KEY ([Invites_InviteId])
    REFERENCES [dbo].[Invites]
        ([InviteId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [UserProfiles_UserId] in table 'UserProfile_Invites'
ALTER TABLE [dbo].[UserProfile_Invites]
ADD CONSTRAINT [FK_UserProfile_Invites_UserProfiles]
    FOREIGN KEY ([UserProfiles_UserId])
    REFERENCES [dbo].[UserProfiles]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserProfile_Invites_UserProfiles'
CREATE INDEX [IX_FK_UserProfile_Invites_UserProfiles]
ON [dbo].[UserProfile_Invites]
    ([UserProfiles_UserId]);
GO

-- Creating foreign key on [Rooms_RoomId] in table 'UserProfile_Room'
ALTER TABLE [dbo].[UserProfile_Room]
ADD CONSTRAINT [FK_UserProfile_Room_Rooms]
    FOREIGN KEY ([Rooms_RoomId])
    REFERENCES [dbo].[Rooms]
        ([RoomId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [UserProfiles_UserId] in table 'UserProfile_Room'
ALTER TABLE [dbo].[UserProfile_Room]
ADD CONSTRAINT [FK_UserProfile_Room_UserProfiles]
    FOREIGN KEY ([UserProfiles_UserId])
    REFERENCES [dbo].[UserProfiles]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserProfile_Room_UserProfiles'
CREATE INDEX [IX_FK_UserProfile_Room_UserProfiles]
ON [dbo].[UserProfile_Room]
    ([UserProfiles_UserId]);
GO

-- Creating foreign key on [UserProfiles_UserId] in table 'webpages_UsersInRoles'
ALTER TABLE [dbo].[webpages_UsersInRoles]
ADD CONSTRAINT [FK_webpages_UsersInRoles_UserProfiles]
    FOREIGN KEY ([UserProfiles_UserId])
    REFERENCES [dbo].[UserProfiles]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [webpages_Roles_RoleId] in table 'webpages_UsersInRoles'
ALTER TABLE [dbo].[webpages_UsersInRoles]
ADD CONSTRAINT [FK_webpages_UsersInRoles_webpages_Roles]
    FOREIGN KEY ([webpages_Roles_RoleId])
    REFERENCES [dbo].[webpages_Roles]
        ([RoleId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_webpages_UsersInRoles_webpages_Roles'
CREATE INDEX [IX_FK_webpages_UsersInRoles_webpages_Roles]
ON [dbo].[webpages_UsersInRoles]
    ([webpages_Roles_RoleId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------