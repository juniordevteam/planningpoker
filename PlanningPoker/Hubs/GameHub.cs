﻿using Microsoft.AspNet.SignalR;
using PlanningPoker.Models;
using PlanningPoker.Repositories;
using PlanningPoker.ViewModels.GameViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlanningPoker.Hubs
{
    public class GameHub : Hub
    {
        private UnitOfWork _unitOfWork = new UnitOfWork();
        private static List<UserInGameViewModel> _users = new List<UserInGameViewModel>();
        private static List<Story> _stories = new List<Story>();
        private static List<EstimateViewModel> _estimates = new List<EstimateViewModel>();

        public void SendMessage(String login, String msg, String roomName)
        {
            Clients.Group(roomName).addMessage(login, msg);
        }

        public void JoinRoom(UserInGameViewModel model)
        {
            var id = Context.ConnectionId;
            var storyList = GetStoryList(model.RoomId);

            if (!_users.Any(u => u.ConnectionId == id))
            {
                model.ConnectionId = id;

                Groups.Add(model.ConnectionId, model.RoomId);
                _users.Add(model);

                var users = _users.Where(u => u.RoomId.Equals(model.RoomId));

                Clients.Caller.onConnected(users, storyList);
                Clients.OthersInGroup(model.RoomId).onNewUserConnected(model);
            }
        }

        public Task LeaveRoom(String roomName)
        {
            return Groups.Remove(Context.ConnectionId, roomName);
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var user = _users.FirstOrDefault(u => u.ConnectionId == Context.ConnectionId);
            if (user != null)
            {
                _users.Remove(user);
                Clients.All.onUserDisconnected(user);
            }

            return base.OnDisconnected(stopCalled);
        }

        #region Timer
        public void StartTimer(String roomName)
        {
            Clients.Group(roomName).startTime();
        }

        public void ResetTimer(String roomName, String value)
        {
            Clients.Group(roomName).resetTime(value);
        }
        #endregion

        #region Story
        public void AddStory(Story storyParam)
        {
            var story = new Story();

            story.StoryId = Guid.NewGuid();
            story.RoomId = storyParam.RoomId;
            story.Title = storyParam.Title;

            _unitOfWork.StoryRepository.Create(story);
            _unitOfWork.Save();

            _stories.Add(story);

            Clients.Group(storyParam.RoomId.ToString()).addStory(story);
        }

        public void SetEstimate(Story storyParam)
        {
            var story = _unitOfWork.StoryRepository.GetById(storyParam.StoryId);

            if (story == null)
            {
                return;
            }

            story.Estimate = storyParam.Estimate;

            _unitOfWork.StoryRepository.Update(story);
            _unitOfWork.Save();

            var storyList = GetStoryList(storyParam.RoomId.ToString());

            Clients.Group(storyParam.RoomId.ToString()).setEstimate(storyList);
        }

        public void DeleteStory(Story storyParam)
        {
            var room = _unitOfWork.RoomRepository.GetById((Int32)storyParam.RoomId);
            var story = _unitOfWork.StoryRepository.GetById(storyParam.StoryId);

            room.Stories.Remove(storyParam);
            _unitOfWork.StoryRepository.Delete(story);
            _unitOfWork.Save();

            _stories.Remove(story);
            var storyList = GetStoryList(storyParam.RoomId.ToString());

            Clients.Group(storyParam.RoomId.ToString()).deleteStory(storyList);
        }

        public IEnumerable<Story> GetStoryList(String roomName)
        {
            var room = _unitOfWork.RoomRepository.GetById(Int32.Parse(roomName));
            var storyList = _unitOfWork.StoryRepository.GetAllForRoom(room).Select(x => new Story
            {
                RoomId = x.RoomId,
                StoryId = x.StoryId,
                Title = x.Title,
                Estimate = x.Estimate
            });

            return storyList;
        }
        #endregion

        public void GetEstimate(EstimateViewModel model)
        {
            if (!_estimates.Any(e => e.OwnerCardId.Equals(model.OwnerCardId)))
            {
                _estimates.Add(model);
            }
            else
            {
                var estimate = _estimates.Where(e => e.OwnerCardId == model.OwnerCardId).Single().Estimate = model.Estimate;
            }
        }

        public void SeeResults(String roomName, String roomOwnerId)
        {
            var users = _users.Where(u => u.RoomId.Equals(roomName) && !u.UserId.Equals(roomOwnerId));
            _estimates.Where(e => e.RoomId.Equals(roomName));
            Clients.Group(roomName).showdown(_estimates, users);
        }

        protected override void Dispose(Boolean disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}